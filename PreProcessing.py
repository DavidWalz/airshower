import numpy as np
import glob
import tensorflow as tf
import norm
import os

Folder = './RawData' # Path to simulated shower packages
if not os.path.exists('Data_preprocessed'):
    os.makedirs('Data_preprocessed')

log_dir = './Data_preprocessed' # Path to preprocessed shower packages
filenames =glob.glob(Folder + "/showers-*")
a = 10000 # packagesize
for i in range(0,len(filenames)):
    data = np.load(filenames[i])
    if i==0:
        signal = data['signal']
        time = data['time']
        logE = data['logE']
        Xmax = data['Xmax']
        showeraxis = data['showeraxis']
        mass = data['mass']
    else:    
        signal = np.append(signal, data['signal'], axis = 0)
        time = np.append(time, data['time'], axis=0)
        logE = np.append(logE, data['logE'], axis=0)
        Xmax = np.append(Xmax, data['Xmax'], axis = 0)
        showeraxis = np.append(showeraxis, data['showeraxis'], axis = 0)
        mass = np.append(mass, data['mass'], axis = 0)

# Normalization of arrival times
time = norm.NormCenter(time)

# Preprocessing of signal
signal[np.less(signal,0)] = 0
SignalSum = norm.NormPhysicalEnergylog10(np.sum(signal, axis=-1))
signal = norm.NormPhysicalTimeTracelog10(signal)

Energy = 10**logE/10**18

# Preprocessed time traces (input1)
Input1 = signal.reshape(len(showeraxis), 9, 9, signal.shape[2], 1)

# Additional preprocessed input maps (arrival times, total signals) (input2)
Input2 = np.stack((time,SignalSum), axis=-1)

# Shuffle data
idx = np.arange(Xmax.shape[0])
np.random.shuffle(idx)
Xmax = Xmax[idx]
showeraxis = showeraxis[idx]
Energy = Energy[idx]
Input1 = Input1[idx]
Input2 = Input2[idx]
mass = mass[idx]

# Store new packages of preprocessed showers
for i in range(len(filenames)):
    np.savez_compressed(log_dir+ "/PlanarWaveShower_PrePro_%i" %i, Xmax=Xmax[a*i:a*(i+1)], Energy = Energy[a*i:a*(i+1)], showeraxis=showeraxis[a*i:a*(i+1)], Input1 = Input1[a*i:a*(i+1)], Input2=Input2[a*i:a*(i+1)], mass=mass[a*i:a*(i+1)])
