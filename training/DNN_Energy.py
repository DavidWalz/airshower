import numpy as np
import keras
import glob
import tensorflow as tf
from keras.callbacks import TensorBoard, EarlyStopping, ReduceLROnPlateau#, NanChecker
from keras.models import Sequential, Model
from keras.layers import *
from keras.optimizers import Adam
import tools

max_steps=15
lr = 0.001
log_dir="."
nbatch = 132

# ----------------------------------------------------------------------
# load and prepare data
# ----------------------------------------------------------------------

Folder = '../Data_preprocessed'
filenames=glob.glob(Folder + "/PlanarWaveShower_PrePro*")

N = 10000*len(filenames)
a = 10000 # packagesize
Input1 = np.zeros(N*9*9*80).reshape(N,9,9,80,1)
Input2 = np.zeros(N*9*9*2).reshape(N,9,9,2)
Energy = np.zeros(N)
for i in range(0,len(filenames)):
    data = np.load(filenames[i])
    Input1[a*i:a*(i+1)] = data['Input1']
    Energy[a*i:a*(i+1)] = data['Energy']
    Input2[a*i:a*(i+1)] = data['Input2']

## Reshape, Split to test and train sets
ntest  = 40000
nval = 2000.
nsplit = N - ntest

Input1, x_test = Input1[:-ntest], Input1[-ntest:]
x_train = Input1
x_train_t0, x_test_t0 = Input2[:-ntest], Input2[-ntest:]
y_train, y_test = Energy[:-ntest], Energy[-ntest:]

## Callbacks & Optimizers
Adam = Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0, clipnorm = 5)
reduceLearning = ReduceLROnPlateau(monitor='val_Distance', factor=0.4, patience=17, verbose=1, mode='min', epsilon=1, cooldown=0, min_lr=0.0)


# ----------------------------------------------------------------------
# Model
# ----------------------------------------------------------------------

kwargs = dict(activation='relu', kernel_initializer='he_normal')
inputTimeTraces = Input(shape=(x_train.shape[1], x_train.shape[2], x_train.shape[3], 1), name='TraceInput') # Input Cube aus time traces
inputT0 = Input(shape=(x_train_t0.shape[1], x_train_t0.shape[2], x_train_t0.shape[3]), name='T0Input') # T0 input
## Time trace characterization
TimeProcess = Conv3D(64, (1,1,7), padding='valid', activation='relu', strides=(1,1,4))(inputTimeTraces)
TimeProcess = Conv3D(32, (1,1,7), padding='valid', activation='relu', strides=(1,1,4))(TimeProcess)
TimeProcess = Conv3D(10, (1,1,4), padding='valid', activation='relu', strides=(1,1,1))(TimeProcess)
TimeProcess = Reshape((9,9,10))(TimeProcess)
x = concatenate([TimeProcess, inputT0])

# Densely connected convolutions
nfilter =12
for i in range(4):
    x = tools.DenselyConnectedSepConv(x, nfilter, **kwargs)
    nfilter = 2*nfilter
x = SeparableConv2D(nfilter, (3, 3), padding = 'same', depth_multiplier=1, **kwargs)(x)
x = Flatten()(x)
output = Dense(1)(x)

model = Model(inputs=[inputTimeTraces, inputT0], outputs=output)
# ----------------------------------------------------------------------

model.compile(loss='mse', optimizer=Adam, metrics=[tools.Distance])
fit = model.fit([x_train, x_train_t0],[y_train], epochs=max_steps, batch_size=nbatch, verbose=1, callbacks=[reduceLearning], validation_split = nval/nsplit)


#------------#
# Evaluation #
#------------#

y_pred = model.predict([x_test, x_test_t0])

# Test evaluation
reco = tools.PlotHistosEnergyReconstruction(y_true=y_test, y_pred=y_pred, log_dir=log_dir, name="test")

## Train evaluation
#y_pred = model.predict([x_train, x_train_t0])
#tools.PlotHistosEnergyReconstruction(y_true=y_train, y_pred=y_pred, log_dir=log_dir, name="train")
