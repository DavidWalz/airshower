import numpy as np
import keras
import glob
import tensorflow as tf
from keras.callbacks import TensorBoard, EarlyStopping, ReduceLROnPlateau#, NanChecker
from keras.models import Sequential, Model
from keras.layers import *
from keras.optimizers import Adam
import tools

max_steps=15
lr = 0.001
log_dir="."
nbatch = 132


# ----------------------------------------------------------------------
# load and prepare data
# ----------------------------------------------------------------------

Folder = '../Data_preprocessed'
filenames=glob.glob(Folder + "/PlanarWaveShower_PrePro*")

N = 10000*len(filenames)
a = 10000 # packagesize
Input2 = np.zeros(N*9*9*2).reshape(N,9,9,2)
Energy = np.zeros(N)
for i in range(0,len(filenames)):
    data = np.load(filenames[i])
    Energy[a*i:a*(i+1)] = data['Energy']
    Input2[a*i:a*(i+1)] = data['Input2'][:,:,:,1].reshape(a, 9, 9, 1)
print('Loaded %i showers' %N )

## Split to test and train sets
ntest  = 40000
nval = 2000.
nsplit = N - ntest

x_train, x_test = np.split(Input2, [nsplit])
y_train, y_test = np.split(Energy, [nsplit])

## Callbacks & Optimizers
Adam = Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0, clipnorm = 2)
reduceLearning = ReduceLROnPlateau(monitor='val_Distance', factor=0.4, patience=17, verbose=1, mode='min', epsilon=1, cooldown=0, min_lr=0.0)


# ----------------------------------------------------------------------
# Model
# ----------------------------------------------------------------------

input1 = Input(shape=Input2.shape[1:])
kwargs = dict(activation='relu', kernel_initializer='he_normal')
x = Conv2D(16, (3, 3), padding = 'same', **kwargs)(input1)
# Densely connected convolutions
nfilter=16
for i in range(4):
    x = tools.DenselyConnectedSepConv(x, nfilter)
    nfilter = 2*nfilter
x = SeparableConv2D(nfilter, (3, 3), padding = 'same', depth_multiplier=1, **kwargs)(x)
x = Flatten()(x)
output = Dense(1)(x)
model = Model([input1], [output])
# ----------------------------------------------------------------------

model.compile(loss='mse', optimizer=Adam, metrics=[tools.Distance])
fit = model.fit(x_train, y_train, batch_size=nbatch, shuffle=True, epochs = max_steps, verbose=1, validation_split=nval/nsplit, callbacks=[reduceLearning]) 


#------------#
# Evaluation #
#------------#

y_pred = model.predict(x_test)
# Test evaluation
reco = tools.PlotHistosEnergyReconstruction(y_true=y_test, y_pred=y_pred, log_dir=log_dir, name="OnlySignal_test")

## Train evaluation
#y_pred = model.predict(x_train)
#tools.PlotHistosEnergyReconstruction(y_true=y_train, y_pred=y_pred, log_dir=log_dir, name="OnlySignal_train")
