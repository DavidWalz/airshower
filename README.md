### A Deep Learning-based Reconstruction of Cosmic Ray-induced Air Showers
##### _Martin Erdmann, Jonas Glombitza, David Walz_

&nbsp;

|  | Website |
| ------ | ------ |
| ArXiv | https://arxiv.org/pdf/1708.00647.pdf |
| Elsevier | https://www.sciencedirect.com/science/article/pii/S0927650517302219?via%3Dihub |

Requirements: Keras, Tensorflow, seaborn, numpy and matplotlib

For training a Deep Neural Network (DNN) on air shower reconstruction, there are 3 steps to do:


  - ##### Simulate data
    Use the script `./sim.py` to simulate showers. (simulates ~100.000 proton showers in 10 packages = 10.000 airshowers per package)
    100.000 showers - 60.000 for training - 40.000 for evaluation
    The data will be saved in `./RawData/showers*`
    
  - ##### Preprocess the simulated data
    Use the script ```PreProcessing.py```
    The script loads all showers in the RAM and preprocess the data.
    The preprocessed data will be stored in `./Data_preprocessed/`

  - ##### Train DNN with preprocessed data
    Start some script in ```./training/```
    After training, the script evaluates the trained model and plots the result. (Default path: `./training/` change path with: log_dir)

```
    Some usefull parameters
    max_steps=15    -   iteration steps (runtime)
    lr = 0.001      -   learning rate
    log_dir="."     -   path to plots
    nbatch = 132    -   size of each mini batch
```

There are 5 DNN's for the 3 reconstruction tasks:

##### Angular Reconstruction
  - Only time input
  - All inputs

##### Energy Reconstruction
  - Only total signal input
  - All inputs

##### Xmax Reconstruction
  - All inputs

For more details on the network architecture see:    https://arxiv.org/pdf/1708.00647.pdf
