import numpy as np
import matplotlib.pyplot as plt


# load data
with np.load('../RawData/showers-A1-0.npz') as data:
    vd = data['detector']
    va = data['showeraxis']
    time = data['time']
    nb_events = len(va)


# reconstruct shower directions with plane wave fit
# cf. ftp://ftp.mi.ingv.it/download/augliera/PER-MARA/article_del_pezzo.pdf
va_rec = np.zeros((nb_events, 3))

for i in range(nb_events):
    m = ~np.isnan(time[i])  # mask for triggered stations
    t = time[i][m]
    x, y, z = vd[m].T

    # distances between all pairs of stations
    idx = np.triu_indices(len(t), k=1)
    dt = np.subtract.outer(t, t)[idx]
    dx = np.subtract.outer(x, x)[idx]
    dy = np.subtract.outer(y, y)[idx]

    # all stations on a plane --> neglect z-position
    dv = np.stack((dx, dy), axis=-1)
    a1 = np.linalg.inv(np.dot(dv.T, dv))

    # # for using z-position as well
    # dz = np.subtract.outer(z, z)[idx]
    # dv = np.stack((dx, dy, dz), axis=-1)
    # a1 = np.linalg.pinv(np.dot(dv.T, dv))

    a2 = np.einsum('ij,i->j', dv, dt)
    a3 = np.einsum('ij,j->i', a1, a2)

    # determine z-component using known speed of propagation = c
    px, py = -a3 * 3E8
    pz = (1 - px**2 - py**2)**.5
    va_rec[i] = [px, py, pz]


# plot angular separation between true and reconstructed angle
ang = np.arccos(np.clip(np.sum(va * va_rec, axis=1), -1, 1)) * 180 / np.pi
r68 = np.percentile(ang, 68)

plt.figure()
plt.hist(ang, bins=np.linspace(0, 5, 41))
plt.axvline(r68, c='r')
plt.xlabel('Angular distance [deg]')
plt.ylabel('\#')
plt.grid()
plt.savefig('fit-planewave.png', bbox_inches='tight')
