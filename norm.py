import numpy as np

def NormCenter(time):
    u = np.isnan(time)
    time -= np.nanmean(time, axis=1)[:,np.newaxis]
    time /= np.nanstd(time)
    time[u] = 0
    try:
        time = time.reshape(time.shape[0],11,11)
    except:
        time = time.reshape(time.shape[0],9,9)
    return time

def NormPhysicalEnergylog10(time):
    u = np.isnan(time)
    time[np.less(time,0)] = 0
    time[~u] = np.log10(time[~u]-np.nanmin(time)+1.0)
    time /= np.nanstd(time)
    time[u] = 0
    try:
        time = time.reshape(time.shape[0], 9, 9)
    except:
        time = time.reshape(time.shape[0], 11,11)
    return time

def NormPhysicalTimeTracelog10(signal, log_dir="."):
    signal[np.less(signal,0)] = 0
    signal = np.log10(signal + 1)
    signal[np.isnan(signal)] = 0
    return signal


