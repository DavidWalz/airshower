import numpy as np
import argparse
import os
from airshower import shower, utils

parser = argparse.ArgumentParser()
parser.add_argument("--mass", default=1, type=int, help='mass number')
parser.add_argument("--nfiles", default=10, type=int, help='number of files')
parser.add_argument("--nevents", default=10000, type=int, help='number of events per file')
parser.add_argument("--path", default='./RawData', type=str, help='output folder')
args = parser.parse_args()

if not(os.path.exists(args.path)):
    os.makedirs(args.path)

print('Simulating')
print('mass', args.mass)
print('nfiles', args.nfiles)
print('nevents', args.nevents)
print('path', args.path)

v_stations = utils.station_coordinates(9, layout='offset')

for i in range(args.nfiles):
    fname = '%s/showers-A%i-%i.npz' % (args.path, args.mass, i)
    print(fname)

    logE = 18.5 + 1.5 * np.random.rand(args.nevents)
    mass = args.mass * np.ones(args.nevents)
    data = shower.rand_events(logE, mass, v_stations)

    np.savez_compressed(
        fname,
        detector=data['detector'],
        logE=data['logE'],
        mass=data['mass'],
        Xmax=data['Xmax'],
        showercore=data['showercore'],
        showeraxis=data['showeraxis'],
        showermax=data['showermax'],
        time=data['time'],
        signal=data['signal'])
